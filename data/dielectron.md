## Нахождение инвариантной массы двух электронов
Данные были взяты с <https://www.kaggle.com/fedesoriano/cern-electron-collision-data>. Данные представляют из себя набор физических значений пары электронов и их инвариантную массу.  

Задача состоит в нахождении инвариантной массы по известным значениям признаков пары электронов.
Список признаков:

1. Run: номер запуска коллайдера, при котором произошло событие;
1. Event: номер события;
1. E1: энергия первого электрона (ГэВ);
1. px1: компонента OX импульса первого электрона;
1. py1: компонента OY импульса первого электрона;
1. pz1: компонента OZ импульса первого электрона;
1. pt1: перпендикулярная компонента импульса первого электрона (transverse moment);
1. eta1: псевдобыстрота первого электрона;
1. phi1: угол наклона первого электрона;
1. Q1: заряд первого электрона;
1. E2: энергия второго электрона (ГэВ);
1. px2: компонента OX импульса второго электрона;
1. py2: компонента OY импульса второго электрона;
1. pz2: компонента OZ импульса второго электрона;
1. pt2: перпендикулярная компонента импульса второго электрона (transverse moment);
1. eta2: псевдобыстрота второго электрона;
1. phi2: угол наклона второго электрона;
1. Q2: заряд второго электрона;
1. **M: инвариантная масса электрона.**  

Целевой признак: M.